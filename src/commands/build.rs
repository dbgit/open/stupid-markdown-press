use comrak::{ComrakExtensionOptions, ComrakOptions, ComrakParseOptions, ComrakRenderOptions};
use eyre::{eyre, Report, Result, WrapErr};

use std::path::{Path, PathBuf};

use crate::lib::files::{create_path_recursive, find_all_markdown_files};
use crate::lib::renderer::{create_website, TranspiledFiles, WebsitePage};
use crate::lib::title_finder;
use crate::opts::BuildOpts;

pub async fn build(opts: BuildOpts) -> Result<(), Report> {
    let root_dir = Path::new(opts.path.as_str());
    let dist_dir = Path::new(opts.out_dir.as_str());

    if let Err(msg) = check_if_path_has_issues(root_dir, false) {
        return Err(msg).wrap_err(format!("Root directory has a problem"));
    }

    if let Err(msg) = check_if_path_has_issues(dist_dir, true) {
        return Err(msg).wrap_err("Dist directory has a problem");
    }

    let mut files: Vec<PathBuf> = vec![];
    if let Err(e) = find_all_markdown_files(&root_dir, &mut files) {
        return Err(e);
    }
    log::info!("Found {} files", files.len());

    let render_options = ComrakOptions {
        extension: ComrakExtensionOptions {
            strikethrough: true,
            tagfilter: false,
            table: true,
            autolink: false,
            tasklist: false,
            superscript: true,
            header_ids: None,
            footnotes: false,
            description_lists: false,
            front_matter_delimiter: None,
        },
        parse: ComrakParseOptions {
            smart: true,
            default_info_string: None,
        },
        render: ComrakRenderOptions::default(),
    };

    let transpiled: Vec<TranspiledFiles> = files
        .into_iter()
        .map(|file| {
            let contents = std::fs::read_to_string(&file).expect("Failed to read file");

            // ToDo: Reuse AST tree
            let title = title_finder::get_document_title(&contents);
            let html = comrak::markdown_to_html(contents.as_str(), &render_options);

            let output_path = match file.strip_prefix(root_dir.to_path_buf()) {
                Ok(t) => t,
                Err(_) => root_dir,
            };

            let mut path = output_path.to_path_buf();
            path.set_extension("html");
            if let Some(file_name) = path.file_name() {
                if file_name.to_str().unwrap().to_lowercase() == "readme.html" {
                    path.set_file_name("index.html")
                }
            }
            TranspiledFiles { title, path, html }
        })
        .collect();

    let rendered = create_website(&transpiled);
    write_rendered_files(dist_dir, rendered)
}

pub fn write_rendered_files(dist_dir: &Path, files: Vec<WebsitePage>) -> Result<(), Report> {
    for file in files {
        let full_path = dist_dir.join(file.path);
        if let Err(e) = create_path_recursive(&full_path) {
            return Err(e);
        }

        if let Err(e) = std::fs::write(&full_path, &file.html) {
            return Err(e).wrap_err(format!("Failed to write file at {:?}", &full_path));
        }
    }

    Ok(())
}

pub fn check_if_path_has_issues(dir: &Path, create: bool) -> Result<(), Report> {
    if !dir.exists() {
        if !create {
            return Err(eyre!("Path does not exist"));
        }
        if let Err(msg) = std::fs::create_dir(dir) {
            return Err(msg).wrap_err(format!("Failed to create directory at {:?}", dir));
        }
    }

    if !dir.is_dir() {
        return Err(eyre!("Path must be a folder"));
    }

    return Ok(());
}
