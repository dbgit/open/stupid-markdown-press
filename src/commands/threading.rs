use comrak::{ComrakExtensionOptions, ComrakOptions, ComrakParseOptions, ComrakRenderOptions};
use eyre::{Report, Result, WrapErr};
use tokio::sync::mpsc::unbounded_channel;
use tokio::sync::Mutex;

use std::path::{Path, PathBuf};
use std::sync::Arc;

use crate::commands::build::{check_if_path_has_issues, write_rendered_files};
use crate::lib::files::find_all_markdown_files;
use crate::lib::renderer::{create_website, TranspiledFiles};
use crate::lib::title_finder;
use crate::opts::ThreadingOpts;

#[derive(Debug)]
enum Message {
    Work { path: PathBuf },
    Write,
    Terminate,
}

#[derive(Debug, Clone)]
struct Response {
    pub title: String,
    pub html: String,
    pub path: PathBuf,
}

const ROOT_PATH: &'static str = ".";
const DIST_PATH: &'static str = "dist";

pub async fn threading_test(_opts: ThreadingOpts) -> Result<(), Report> {
    log::info!("Validating Input");
    let root_dir = Path::new(ROOT_PATH);
    let dist_dir = Path::new(DIST_PATH);

    if let Err(msg) = check_if_path_has_issues(root_dir, false) {
        return Err(msg).wrap_err(format!("Root directory has a problem"));
    }

    if let Err(msg) = check_if_path_has_issues(dist_dir, true) {
        return Err(msg).wrap_err("Dist directory has a problem");
    }

    let root_dir_arc = Arc::new(root_dir);
    let dist_dir_arc = Arc::new(dist_dir);

    log::info!("Preparing Threads");
    let size = num_cpus::get();
    let (sender, receiver) = unbounded_channel();
    let receiver = Arc::new(Mutex::new(receiver));
    let write_lock = Arc::new(Mutex::new(size));

    let mut workers = Vec::with_capacity(size);

    log::info!("Starting Threads");
    for id in 0..size {
        let receiver = Arc::clone(&receiver);
        let write_lock = Arc::clone(&write_lock);
        let root_dir = Arc::clone(&root_dir_arc);
        let dist_dir = Arc::clone(&dist_dir_arc);
        let worker = tokio::spawn(async move {
            let mut results: Vec<TranspiledFiles> = vec![];

            let render_options = ComrakOptions {
                extension: ComrakExtensionOptions {
                    strikethrough: true,
                    tagfilter: false,
                    table: true,
                    autolink: false,
                    tasklist: false,
                    superscript: true,
                    header_ids: None,
                    footnotes: false,
                    description_lists: false,
                    front_matter_delimiter: None,
                },
                parse: ComrakParseOptions {
                    smart: true,
                    default_info_string: None,
                },
                render: ComrakRenderOptions::default(),
            };

            loop {
                let message = receiver
                    .lock()
                    .await
                    .recv()
                    .await
                    .unwrap_or_else(|| Message::Terminate);

                match message {
                    Message::Work { path: amount } => {
                        let contents =
                            std::fs::read_to_string(&amount).expect("Failed to read file");

                        // ToDo: Reuse AST tree
                        let title = title_finder::get_document_title(&contents);
                        let html = comrak::markdown_to_html(contents.as_str(), &render_options);

                        let output_path = match amount.strip_prefix(&root_dir.to_path_buf()) {
                            Ok(t) => t,
                            Err(_) => &root_dir,
                        };

                        let mut path = output_path.to_path_buf();
                        path.set_extension("html");
                        if let Some(file_name) = path.file_name() {
                            if file_name.to_str().unwrap().to_lowercase() == "readme.html" {
                                path.set_file_name("index.html")
                            }
                        }
                        results.push(TranspiledFiles { title, path, html });
                    }
                    Message::Write => {
                        // log::info!("{} - Waiting for write lock", id);
                        let lock = write_lock.lock().await;
                        log::info!("Thread {} writing {} files", id, results.len());
                        let rendered = create_website(&results);
                        if let Err(e) = write_rendered_files(&dist_dir, rendered) {
                            panic!(e);
                        }

                        drop(lock);
                    }
                    Message::Terminate => break,
                }
            }
        });
        workers.push(worker);
    }

    log::info!("Finding Files");
    let mut files: Vec<PathBuf> = vec![];
    if let Err(e) = find_all_markdown_files(&root_dir, &mut files) {
        return Err(e);
    }
    log::info!("Found {} files", files.len());

    log::info!("Starting Work");
    for i in files {
        sender.send(Message::Work { path: i }).unwrap();
    }

    log::info!("Writing");
    for _ in &workers {
        sender.send(Message::Write).unwrap();
        sender.send(Message::Terminate).unwrap();
    }

    // log::info!("Terminating Threads");
    // for _ in &workers {
    //   sender.send(Message::Terminate).unwrap();
    // }

    log::info!("Waiting for threads to finish");
    for worker in workers {
        if let Err(e) = worker.await {
            panic!(e);
        }
    }

    log::info!("Done");

    Ok(())
}
