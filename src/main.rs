mod commands;
mod lib;
mod opts;

use clap::Clap;
use eyre::{Report, Result};

use commands::build;
use opts::*;

#[tokio::main]
pub async fn main() -> Result<(), Report> {
    color_eyre::install()?;
    lib::logger::init();

    let opts: Opts = Opts::parse();
    match opts.sub_commands {
        SubCommand::Build(opts) => build::build(opts).await,
        SubCommand::Threading(opts) => commands::threading::threading_test(opts).await,
        SubCommand::Completions(opts) => crate::generate_completions(opts),
    }
}
