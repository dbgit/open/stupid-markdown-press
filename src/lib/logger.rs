use env_logger::fmt::Color;
use env_logger::Target;
use log::{Level, LevelFilter};

use std::io::Write;

pub fn init() {
    env_logger::builder()
        .target(Target::Stdout)
        .filter_level(LevelFilter::Info)
        .format(|buf, record| {
            let level = record.level();
            let color = match level {
                Level::Error => Color::Red,
                Level::Warn => Color::Yellow,
                Level::Info => Color::Blue,
                Level::Debug => Color::Magenta,
                Level::Trace => Color::White,
            };

            writeln!(
                buf,
                "[{} {}] {}",
                buf.timestamp(),
                buf.style().set_color(color).set_bold(true).value(level),
                record.args()
            )
        })
        .init();
}
