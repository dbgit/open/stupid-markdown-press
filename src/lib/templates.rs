use crate::lib::renderer::FileIndex;
use sailfish::TemplateOnce;

#[derive(TemplateOnce)]
#[template(path = "page.stpl")]
pub struct IndexTemplate {
    pub files: Vec<FileIndex>,
    pub title: String,
    pub content: String,
}
