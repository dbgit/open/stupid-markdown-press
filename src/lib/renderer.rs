use sailfish::TemplateOnce;

use std::path::PathBuf;

use crate::lib::templates::IndexTemplate;

#[derive(Debug)]
pub struct TranspiledFiles {
    pub title: String,
    pub html: String,
    pub path: PathBuf,
}

pub struct WebsitePage {
    pub path: PathBuf,
    pub html: String,
}

#[derive(Debug, Clone)]
pub struct FileIndex {
    pub name: String,
    pub path: String,
}

pub fn create_website(files: &Vec<TranspiledFiles>) -> Vec<WebsitePage> {
    let file_index = create_file_index(&files);

    files
        .into_iter()
        .map(|file| {
            let html = create_full_document(&file_index, file.title.clone(), file.html.clone());

            WebsitePage {
                path: file.path.clone(),
                html,
            }
        })
        .collect()
}

fn create_file_index(files: &Vec<TranspiledFiles>) -> Vec<FileIndex> {
    files
        .into_iter()
        .map(|file| FileIndex {
            name: file.title.clone(),
            path: convert_path(file.path.clone().to_str().unwrap()),
        })
        .collect()
}

#[cfg(windows)]
fn convert_path(p: &str) -> String {
    p.replace(std::path::MAIN_SEPARATOR, "/").to_string()
}

#[cfg(unix)]
fn convert_path(p: &str) -> String {
    p.to_string()
}

fn create_full_document(file_index: &Vec<FileIndex>, title: String, html: String) -> String {
    IndexTemplate {
        files: file_index.clone(),
        title,
        content: html,
    }
    .render_once()
    .unwrap()
}
