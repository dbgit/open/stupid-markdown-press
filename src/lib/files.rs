use eyre::{Report, Result, WrapErr};
use std::fs;
use std::path::{Path, PathBuf};

pub fn create_path_recursive(path: &Path) -> Result<(), Report> {
    let parent = match path.parent() {
        Some(parent) => parent,
        None => return Ok(()),
    };

    if parent.exists() {
        return Ok(());
    }

    if let Err(e) = create_path_recursive(parent) {
        return Err(e);
    }

    match fs::create_dir(&parent) {
        Ok(_) => Ok(()),
        Err(e) => Err(e).wrap_err(format!("Failed to create folder at: {:?}", parent)),
    }
}

pub fn find_all_markdown_files(path: &Path, collector: &mut Vec<PathBuf>) -> Result<(), Report> {
    if !path.exists() {
        return Ok(());
    }

    let files = match fs::read_dir(path) {
        Ok(f) => f,
        Err(e) => return Err(e).wrap_err(format!("Failed to read directory {:?}", path)),
    };

    for file in files {
        let entry = file.unwrap().path();
        if entry.is_dir() {
            if let Err(e) = find_all_markdown_files(&entry, collector) {
                return Err(e);
            }
        } else if let Some(ext) = entry.extension() {
            if ext == "md" {
                collector.push(entry)
            }
        }
    }

    Ok(())
}
