use clap::{Clap, IntoApp};
use clap_generate::generators::{Bash, Elvish, Fish, PowerShell, Zsh};
use clap_generate::{generate, Generator};
use eyre::{eyre, Report, Result};

#[derive(Clap)]
#[clap(version = "0.1", author = "Daan Boerlage <d@boerlage.me>")]
pub struct Opts {
    #[clap(subcommand)]
    pub sub_commands: SubCommand,
}

#[derive(Clap)]
pub enum SubCommand {
    #[clap(about = "Build current repo")]
    Build(BuildOpts),
    Threading(ThreadingOpts),
    #[clap(about = "Generate shell completions")]
    Completions(CompletionOpts)
}

#[derive(Clap)]
pub struct BuildOpts {
    #[clap(about = "Root folder of the repo to build", default_value = ".")]
    pub path: String,
    #[clap(
        short = 'o',
        long = "--out-dir",
        default_value = "dist",
        about = "Output directory of the build"
    )]
    pub out_dir: String,
}

#[derive(Clap, Clone, Debug)]
pub struct ThreadingOpts {
    #[clap(about = "Root folder of the repo to build", default_value = ".")]
    pub path: String,
    #[clap(
        short = 'o',
        long = "--out-dir",
        default_value = "dist",
        about = "Output directory of the build"
    )]
    pub out_dir: String,
}

#[derive(Clap)]
pub struct CompletionOpts {
    #[clap(about = "list globally ignored files")]
    pub shell: String,
}

fn print_completions<G: Generator>() {
    let mut app = Opts::into_app();
    let name = app.get_name().to_string();
    generate::<G, _>(&mut app, &name, &mut std::io::stdout())
}

pub fn generate_completions(opts: CompletionOpts) -> Result<(), Report> {
    let shell = opts.shell.as_str();
    match shell {
        "bash" => print_completions::<Bash>(),
        "elvish" => print_completions::<Elvish>(),
        "fish" => print_completions::<Fish>(),
        "powershell" => print_completions::<PowerShell>(),
        "pwsh" => print_completions::<PowerShell>(),
        "zsh" => print_completions::<Zsh>(),
        _ => return Err(eyre!("Unknown generator")),
    }

    Ok(())
}
